
const student = {
   name: "",
   lastName: "",
};

student.name = prompt("Введіть ім'я студента");
student.lastName = prompt("Введіть прізвище студента");


student.table = {};
do {
   stdClass = prompt("Введіть назву предмета");
   if (typeof stdClass !== "object") {
      student.table[stdClass] = +prompt(`Введіть оцінку з предмету ${stdClass}`, "Від 1 до 12");
   }
} while (typeof stdClass !== "object");


let badRating = [];
for (let stdClass in student.table) {
   if (student.table[stdClass] < 4) {
      badRating.push(student.table[stdClass])
   }
};

if (badRating.length === 0) {
   alert(`Студента ${student.lastName} ${student.name} переведено на наступний курс`)
} else {
   alert(`Студент ${student.lastName} ${student.name} має незадовільні оцінки у ${badRating.length} предметі(ах)`)
};


let generalRating = [];
for (let stdClass in student.table) {
   generalRating.push(student.table[stdClass])
};

let middleRating = (generalRating.reduce(function (a, b) { return a + b; })) / generalRating.length;
if (middleRating >= 7) {
   alert(`Студенту ${student.lastName} ${student.name} призначено стипендію, середній бал - ${middleRating}`)
} else {
   alert(`Студент ${student.lastName} ${student.name} має середній бал ${middleRating}, нижче 7-ми`)
}

console.log(student, middleRating);